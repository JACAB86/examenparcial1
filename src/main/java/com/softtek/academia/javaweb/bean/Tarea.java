package com.softtek.academia.javaweb.bean;

public class Tarea {

	Long id;
	String list;
	boolean _isDone;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getList() {
		return list;
	}
	public void setList(String list) {
		this.list = list;
	}
	public boolean is_isDone() {
		return _isDone;
	}
	public void set_isDone(boolean _isDone) {
		this._isDone = _isDone;
	} 
	
	
}
