package com.softtek.academia.javaweb.bean;

import com.softtek.academia.javaweb.dao.AgregarActividad;
import com.softtek.academia.javaweb.dao.TareaDao;

public class BeanInsert {
	String tarea;
	boolean done;
	
	
	public String getTarea() {
		return tarea;
	}
	public void setTarea(String tarea) {
		this.tarea = tarea;
	}
	public boolean isDone() {
		return done;
	}
	public void setDone(boolean done) {
		this.done = done;
	}
	
	public void addActivity(String activity) {
		 AgregarActividad add = new AgregarActividad(activity);
	}
	
	public boolean validate(String actividad) {
		boolean result = TareaDao.Agregar(actividad);
		if(result) {
			return true;
		}else {
			return false;
		}
	}
}
