package com.softtek.academia.javaweb.controller;

import java.awt.List;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.activation.DataSource;
import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.softtek.academia.javaweb.bean.Tarea;
import com.softtek.academia.javaweb.dao.AgregarActividad;
import com.softtek.academia.javaweb.dao.TareaDao;

/**
 * Servlet implementation class ControllerServlet2
 */

@WebServlet("/tareaDao")
public class ControllerServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */

    public ControllerServlet2() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String uname = request.getParameter("Done");
		AgregarActividad addTarea = new AgregarActividad();
		addTarea.cambiarStatus(uname); 
		RequestDispatcher rd = request.getRequestDispatcher("views/DoneList.jsp");
		rd.forward(request, response);

	}

}
