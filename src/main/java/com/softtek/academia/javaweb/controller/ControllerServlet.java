package com.softtek.academia.javaweb.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academia.javaweb.bean.BeanInsert;

/**
 * Servlet implementation class ControllerServlet
 */
public class ControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControllerServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
		/*boolean status = bean.validate(bean);
			
			
			if(status) {
				RequestDispatcher rd = request.getRequestDispatcher("views/login-success.jsp");
				rd.forward(request, response);
			} else {
				RequestDispatcher rd = request.getRequestDispatcher("views/login-unsuccessfull.jsp");
				rd.forward(request, response);
			}
		*/
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String activity = request.getParameter("activity");
		System.out.println(activity);
	
		BeanInsert bean = new BeanInsert();
		request.setAttribute("bean", bean);
	
		bean.setTarea(activity);
		doGet(request, response);
		boolean status = bean.validate(activity);
		
		if(status) {
			RequestDispatcher rd = request.getRequestDispatcher("views/MenuPage.jsp");
			rd.forward(request, response);
		
		System.out.println("Aprobado");
		} else {
			RequestDispatcher rd = request.getRequestDispatcher("views/MenuPage.jsp");
			rd.forward(request, response);
			System.out.println("No aprobado");

		}
		
	}

}
