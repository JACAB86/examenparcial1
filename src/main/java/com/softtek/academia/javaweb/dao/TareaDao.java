package com.softtek.academia.javaweb.dao;

import java.awt.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.activation.DataSource;

import com.softtek.academia.javaweb.bean.BeanInsert;
import com.softtek.academia.javaweb.bean.Tarea;

public class TareaDao {
	
	private DataBase dataSource;
	ArrayList<Tarea> tareas;
	
	public TareaDao() {
		
    }
	
	public static Connection getConnection() {
		final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
		final String DB_URL = "jdbc:mysql://localhost:3306/sesion3"+"?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		final String USER = "root";
		final String PASS = "1234";
		
		Connection con = null;
		try {
		Class.forName(JDBC_DRIVER);
		con = DriverManager.getConnection(DB_URL,USER,PASS);
		}catch(Exception e) {
			System.out.println(e);
		}
		return con;
		
}
	
	public ArrayList<Tarea> getActivities() { 
		
		try {
			Connection conn = getConnection();
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM to_do_list"); 
			ResultSet rs = ps.executeQuery(); 
			
			while(rs.next()) { 
				Tarea tarea = new Tarea();	
				tarea.setId(rs.getLong("id"));
				tarea.setList(rs.getString("list"));
				tarea.set_isDone(rs.getBoolean("is_done"));
				System.out.println("Entro un registro!!!");
				tareas.add(tarea);
				} 
			return tareas;
		}catch(Exception e) { 
			System.out.println("Raios"+ e);
		return tareas; 
		} 
} 
	
	public static boolean Agregar(String actividad) {
		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement("INSERT INTO to_do_list (list,is_done) VALUES (?,?)");
			System.out.println("Llega");
			ps.setString(1,actividad);
			System.out.println("Llega");
			ps.setInt(2, 0);
			System.out.println("Llega");
			ps.executeUpdate();
			System.out.println("Insertado");
			return true;
				/*
				String actividad2 =rs.getString("list"); //Nombres columnas tabla
				System.out.println(actividad2);
				*/
			
		}catch(Exception e) {
			System.out.println(e);
			return false;

		}
		
	}
		 
	
}
