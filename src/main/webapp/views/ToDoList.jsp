<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>

    <%
    	String  contextPath = request.getContextPath();
    %>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
    
    
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSP List Users Records</title>
</head>
<body>

	<sql:setDataSource var="con" driver="com.mysql.jdbc.Driver" url="jdbc:mysql://localhost:3306/sesion3?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC" user="root" password="1234"/>
	<sql:query dataSource="${con }" sql="select * from to_do_list where is_done is false" var="result"/>
	<table border="2" width="70%" cellpadding="2"> 
		<tr>
			<th>ID</th>
			<th>Tarea</th>
			<th>Status</th>
			<th>Change Status</th>
			</tr>
			<c:forEach var="act" items="${result.rows }"> 
                <tr>
					<td>${act.id}</td>
					<td>${act.list}</td>
					<c:if test="${act.is_done == true}">
         				<td>Done</td>
     				</c:if>
     				<c:if test="${act.is_done == false}">
         				<td>Not Done</td>
     				</c:if>
					<td>

						<form action="../controllerServlet2Example" method="POST">
							<button name="Done" type="submit" value=${act.id}>Done!!!</button>
						</form>

					</td>
				</tr> 
            </c:forEach>
	</table> 
	<li><a href="<%=contextPath %>/views/MenuPage.jsp">Go To Menu Page</a></li> 
  
 

</body>
</html>
