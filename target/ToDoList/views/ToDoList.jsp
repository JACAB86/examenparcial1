<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSP List Users Records</title>
</head>
<body>
	<table border="2" width="70%" cellpadding="2"> 
		<tr>
			<th>ID</th>
			<th>Tarea</th>
			<th>Status</th>
			</tr>
			<c:forEach var="act" items="${list}"> 
                <tr>
					<td>${act.id}</td>
					<td>${act.list}</td>
					<td>${act.is_done}</td>
				</tr> 
            </c:forEach>
	</table>   
 

</body>
</html>
